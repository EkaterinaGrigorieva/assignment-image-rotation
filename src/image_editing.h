#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

#ifndef IMAGE_EDITING_H
#define IMAGE_EDITING_H

struct image rotate_90_right(struct image const image1);

#endif
