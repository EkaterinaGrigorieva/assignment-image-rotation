#include <stdlib.h>
#include <stdio.h>
#include "image_struct.h"

struct image * bmp_image_malloc(){
   return (struct image *) malloc(sizeof(struct image));
}

void bmp_free(struct image* image1){
    free((*image1).data);
}

struct pixel* image_data_padding_malloc(struct image* image1, uint32_t k_padding) {
	return (struct pixel*) malloc( (*image1).height *((*image1).width* sizeof(struct pixel) + k_padding));
}

struct pixel* image_data_malloc(struct image* image1) {
	return (struct pixel*) malloc(sizeof(struct pixel)*(*image1).width*(*image1).height);
}

