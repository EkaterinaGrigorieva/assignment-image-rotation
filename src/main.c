#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include "bmp_utils.h"
#include "image_struct.h"
#include "file_utils.h"
#include "malloc_free.h"
#include "image_editing.h"
#include "utils.h"

void usage() {
    fprintf(stderr, "Usage: ./print_header BMP_FILE_NAME\n"); 
}

static const char* file_status_string[] = {
	  [OPEN_FOR_READ_OK] = "Opening file for reading - OK",
	  [OPEN_FOR_READ_ERROR] = "Error opening file for reading",
	  [OPEN_FOR_WRITE_OK] = "Opening file for writing - OK",
	  [OPEN_FOR_WRITE_ERROR] = "Error opening file for writing",
	  [CLOSE_OK] = "Closing the file - Ok",
	  [C_OTHER] = "Undefined error. I don't know what is going on :O"
};

static const char* bmp_status_string[] = {
	  [BMP_READ_OK] = "Reading image from file - OK",
	  [BMP_READ_INVALID_FILE_SIGNATURE] = "Error: invalid file signature",
	  [BMP_READ_INVALID_FILE_BITS] = "Error: invalid file bits",
	  [BMP_READ_INVALID_FILE_HEADER] = "Error: invalid file header",
	  [BMP_READ_INVALID_FILE_PATH] = "Error: path not found",
	  [BMP_WRITE_OK] = "Saving image in file - OK",
	  [BMP_WRITE_ERROR] = "Error saving image in file",
	  [BMP_OTHER] = "Undefined error. I don't know what is going on :O"
};

int main( int argc, char** argv ) {
    
    if (argc != 3) usage();
    if (argc < 2) err("Not enough arguments \n" );

    const char *in_path = argv[1];
    char *out_path;
    
    if (argc > 2) {
    	out_path = argv[2];
    }
    else {
        out_path = "out.bmp";
    }
    
    FILE * in_file = NULL;
    
    struct image pic_config = {0};
    
    enum file_status file_current_status = open_for_read(&in_file, in_path);
    fprintf(stderr, "%s \n", file_status_string[file_current_status]);
   
    enum bmp_status bmp_current_status = read_bmp_image(in_file, &pic_config);
    fprintf(stderr, "%s \n", bmp_status_string[bmp_current_status]);
    file_current_status = file_close(in_file);
    fprintf(stderr, "%s \n", file_status_string[file_current_status]);

    FILE * out_file = NULL;
    file_current_status = open_for_write(&out_file, out_path);
    fprintf(stderr, "%s \n", file_status_string[file_current_status]);
    
    struct image edited_image = rotate_90_right(pic_config);
    bmp_free(&pic_config);
    
    bmp_current_status = write_bmp_image(out_file, &edited_image);
    fprintf(stderr, "%s \n", bmp_status_string[bmp_current_status]);
    
    file_current_status = file_close(out_file);
    fprintf(stderr, "%s \n", file_status_string[file_current_status]);
    
    bmp_free(&edited_image);
    
    return 0;
}
