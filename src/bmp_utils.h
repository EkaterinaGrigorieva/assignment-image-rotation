#ifndef BMP_UTILS_H
#define BMP_UTILS_H

#include "image_struct.h"
#include "stdio.h"
#include <stdint.h>

enum bmp_status {
    BMP_READ_OK = 0,
    BMP_READ_INVALID_FILE_SIGNATURE,
    BMP_READ_INVALID_FILE_BITS,
    BMP_READ_INVALID_FILE_HEADER,
    BMP_READ_INVALID_FILE_PATH,
    BMP_WRITE_OK,
    BMP_WRITE_ERROR,
    BMP_OTHER
};

enum bmp_status read_bmp_image(FILE* file, struct image* image1);
enum bmp_status write_bmp_image(FILE* file, struct image* image1);

#endif
