#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "image_struct.h"
#include "malloc_free.h"
#include "bmp_utils.h"


struct image rotate_90_right(struct image const image1) {

    struct image new_image = {.width = image1.height, .height = image1.width};
	new_image.data = image_data_malloc(&new_image);

	for (uint64_t h = 0; h < image1.height; h = h + 1) {
		for (uint64_t w = 0; w < image1.width; w = w + 1) {
			new_image.data[((new_image.height-w-1)*new_image.width)+h] = image1.data[h*image1.width+w];
		}
	}

	return new_image; 
}