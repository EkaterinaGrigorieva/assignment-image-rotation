#ifndef MALLOC_FREE_H
#define MALLOC_FREE_H
#include "image_struct.h"

struct image * bmp_image_malloc();
void bmp_free(struct image* image1);
struct pixel* image_data_padding_malloc(struct image* image1, uint32_t k_padding);
struct pixel* image_data_malloc(struct image* image1);

#endif

