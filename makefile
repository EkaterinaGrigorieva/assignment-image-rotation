CFLAGS = --std=c18 -Isrc/ -pedantic -Wall -Werror
APP = build/BMP 

C_SRC_FILES = $(wildcard src/*.c)
OBJ_FILES = $(addprefix build/,$(notdir $(C_SRC_FILES:.c=.o)))

.PHONY: all clean

define build-obj
	gcc -c $(CFLAGS) $< -o $@ 
endef

all: $(APP)

$(APP): $(OBJ_FILES)
	gcc -o $(APP) $^

$(OBJ_FILES): build/%.o: src/%.c
	mkdir -p build/
	$(call build-obj) 

clean:
	rm  -rf build/*

