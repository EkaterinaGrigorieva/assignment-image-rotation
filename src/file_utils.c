#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdarg.h>
#include "file_utils.h"


enum file_status open_for_read(FILE ** file, const char *file_path){
    *file = fopen(file_path, "rb");
    if (*file) {
         return OPEN_FOR_READ_OK;
    }
    return OPEN_FOR_READ_ERROR;
}

enum file_status open_for_write(FILE ** file, const char *file_path){
    *file = fopen(file_path, "wb");
    if (*file) {
         return OPEN_FOR_WRITE_OK;
    }
    return OPEN_FOR_WRITE_ERROR;
}
    

enum file_status file_close(FILE * file) {
    fclose(file);
    return CLOSE_OK;
}


