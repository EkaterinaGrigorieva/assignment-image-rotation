#include <stdlib.h>
#include <stdio.h>
#include "image_struct.h"
#include "bmp_utils.h"
#include "malloc_free.h"

#define BMP_BF_TYPE 0x4D42

static uint32_t k_padding_calculate(uint32_t width) {

	return (4 - (width * sizeof(struct pixel)) % 4) % 4;
	
}

struct __attribute__((packed)) bmp_header {
        uint16_t bfType;
        uint32_t  bfileSize;
        uint32_t bfReserved;
        uint32_t bOffBits;
        uint32_t biSize;
        uint32_t biWidth;
        uint32_t  biHeight;
        uint16_t  biPlanes;
        uint16_t biBitCount;
        uint32_t biCompression;
        uint32_t biSizeImage;
        uint32_t biXPelsPerMeter;
        uint32_t biYPelsPerMeter;
        uint32_t biClrUsed;
        uint32_t  biClrImportant;
};

static struct bmp_header new_header (struct image const *image1) {

    const uint32_t k_padding = k_padding_calculate((*image1).width);
    
    struct bmp_header header = {
    .bfType = BMP_BF_TYPE,
    .bfileSize = sizeof(struct bmp_header) + ((*image1).width * sizeof(struct pixel) + k_padding)*(*image1).height,
    .bfReserved = 0,
    .bOffBits = sizeof(struct bmp_header),
    .biSize = 40,
    .biWidth = (*image1).width,
    .biHeight = (*image1).height,
    .biPlanes = 1,
    .biBitCount = 24,
    .biCompression = 0,
    .biXPelsPerMeter = 0,
    .biYPelsPerMeter = 0,
    .biClrUsed = 0,
    .biClrImportant = 0};
    header.biSizeImage = ((*image1).width* sizeof(struct pixel)  + k_padding)*(*image1).height;
    return header;
}

static enum bmp_status header_check (struct bmp_header header) {

	if (header.bfType != BMP_BF_TYPE) { 
		return BMP_READ_INVALID_FILE_SIGNATURE; 
	}
	
	if (header.bfileSize !=  header.biSizeImage + header.bOffBits){
                return BMP_READ_INVALID_FILE_HEADER;
        }
	
	return BMP_READ_OK;
}

enum bmp_status read_bmp_image(FILE* file, struct image* image1) {
	
    	struct bmp_header header = {0};
    	
        if (file == NULL) {
            return BMP_READ_INVALID_FILE_PATH;
        }
            
        fread(&header, sizeof(struct bmp_header), 1, file);
        const enum bmp_status read_header = header_check(header);
        if (read_header != BMP_READ_OK) {
            return read_header;
        }

        if (header.biSizeImage == 0) {
		header.biSizeImage = header.bfileSize;
        }
	
    	const uint32_t k_padding = k_padding_calculate(header.biWidth);
    
        (*image1).width = header.biWidth;
        (*image1).height = header.biHeight;
        (*image1).data = image_data_padding_malloc(image1, k_padding);
    
        for (uint64_t h = 0; h < (*image1).height; h = h + 1) {
            if(fread((*image1).data + h*(*image1).width, sizeof(struct pixel), (*image1).width, file) != (*image1).width) {
                if(!feof(file)) {
                	free((*image1).data);
                	return BMP_READ_INVALID_FILE_BITS;
                }
            }
	        fseek(file, k_padding, SEEK_CUR);
        }
    
        return BMP_READ_OK;
}

enum bmp_status write_bmp_image(FILE* file, struct image* image1) {
    
    if (file == NULL) {
		return BMP_READ_INVALID_FILE_PATH;
    }
    	
    const uint32_t k_padding = k_padding_calculate((*image1).width);
    
    struct bmp_header new_header1 = new_header(image1);
    
    if (fwrite(&new_header1, sizeof(struct bmp_header), 1, file) != 1) {
    	return BMP_WRITE_ERROR;
    }
    
    for (uint64_t h = 0; h < (*image1).height; h = h + 1) {
        if( fwrite((*image1).data + h*(*image1).width, sizeof(struct pixel), (*image1).width, file) != (*image1).width) {
        	return BMP_WRITE_ERROR;
        }
	fseek(file, k_padding, SEEK_CUR);
    }
    
    return BMP_WRITE_OK;
}
