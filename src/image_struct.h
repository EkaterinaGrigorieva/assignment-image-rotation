#include <stdint.h>
#ifndef IMAGE_STRUCT_H
#define IMAGE_STRUCT_H

struct pixel {uint8_t b, g, r;};

struct image {
    uint64_t width, height;
    struct pixel* data;
};

#endif
