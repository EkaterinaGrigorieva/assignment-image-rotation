#ifndef FILE_UTILS_H
#define FILE_UTILS_H
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>


enum file_status {
    OPEN_FOR_READ_OK = 0,
    OPEN_FOR_READ_ERROR,
    OPEN_FOR_WRITE_OK,
    OPEN_FOR_WRITE_ERROR,
    CLOSE_OK,
    C_OTHER
};

enum file_status open_for_read(FILE ** file, const char *file_path);
enum file_status open_for_write(FILE ** file, const char *file_path);
enum file_status file_close(FILE * file);

uint8_t print_status(enum file_status status);

#endif
